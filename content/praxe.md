---
title: Praxe
menu:
  main:
    parent: about
---
## PRACOVNÍ ZKUŠENOSTI
* Rehabilitace MARIENKA soukromá praxe (fyzioterapeutka) 2023 – současnost
* Centrum léčebné rehabilitace Brno (fyzioterapeutka) 2021 – současnost; 2012 – 2015
* Fakulta sportovních studií MU v Brně, obor: Fyzioterapie (učitelka) 2021 – 2022
* King Faisal Specialist Hospital & Research Center Jeddah (fyzioterapeutka, lektorka) 2015 – 2020
* ParaCentrum Fénix Brno (fyzioterapeutka) 2009 – 2010
* Kontakt bez bariér Brno (plavecká instruktorka, ošetřovatelka, fyzioterapeutka) 2007 – 2010
* Hospic svaté Alžběty Brno (dobrovolnice) 2005 – 2007

## VZDĚLÁNÍ
* Fakulta tělesné kultury Univerzity Palackého (obor Fyzioterapie, magisterské studium) 2010 – 2012
* Lékařská fakulta Masarykovy univerzity (obor Fyzioterapie, bakalářské studium) 2006 – 2010

