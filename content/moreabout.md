---
title: Více o mně
menu:
  main:
    parent: about
---
## CO VÁM MOHU POMOCI VYŘEŠIT
- Vaše akutní i chronické bolesti pohybového aparátu
- Bolesti ramene
- Dlouhodobé bolesti krční oblasti
- Bolesti čelistního kloubu
- Přetrvávající dechové potíže po COVID onemocnění
- Bolesti u silového tréninku i jiných sportů
- Prevenci zranění při sportovní zátěži
- Prevenci sedavého zaměstnání
- Kompenzaci skoliózy či skoliotického držení těla
- Optimalizaci vašeho domácího cvičení
- U akutních stavů co nejrychlejší návrat do normálu
