---
title: Kontakt
menu:
  main:
    weight: 10
---
- Mgr. Marie Krejčová
- Adresa Sirotkova 43, Brno – Žabovřesky, 616 00
- ✉️ marienka.k@seznam.cz
- ☎️ [+420 774 66 28 02](tel:+420774662802)
- IČ: 19392389
- Bankovní spojení: 2100917122/2010
