---
title: Ceník
menu:
  main:
    weight: 200
---
# CENÍK & SLUŽBY

{{<table "table table-striped table-hover">}}
| Služba | Cena |
| -------- | ------- |
| Vstupní komplexní vyšetření pohybového aparátu vč. terapie 70 min. | 1000 Kč / 1200 Kč platné od 01.04.2025 |
| Komplexní terapie (pokračování) 55 min. | 800 Kč / 1000 Kč platné od 01.04.2025 |
| Komplexní terapie (pokračování) 25 min. | 500 Kč / 600 Kč platné od 01.04.2025 |
| Kineziotejping v rámci terapie | 1 Kč/cm |
| Kineziotejping bez terapie | 150 Kč diagnostika + 2 Kč/cm / 200 Kč diagnostika + 2 Kč platné od 01.04.2025 |
{{</table>}}

## PODMÍNKY ZRUŠENÍ TERAPIE

Vážení klienti Rehabilitace Marienka, chtěla bych vás požádat, abyste svoji terapii rušili maximálně 24 hodin před jejím zahájením. V opačném případě vám bude účtován poplatek v hodnotě 200,-Kč. Domluvené termíny jsou závazné.

Terapii lze zrušit pomocí SMS, emailu či WhatsUpu. Nicméně pro zrušení prosím nevolejte! Mějte prosím na paměti, že dělám manuální práci a během svých terapií telefon neberu. Pokud se chcete objednat na svou první terapii můžete využít email či telefon. Pokud telefon neberu, zavolám vám zpátky, jakmile to bude možné. Děkuji moc, že respektujete podmínky ordinace.

## ZMĚNA CENÍKU
Vážení klienti Rehabilitace Marienka, od 01.04.2025 dojde ke změnám výše úhrad za fyzioterapii. Děkuji za pochopení!
