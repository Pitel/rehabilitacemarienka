---
title: Certifikace
menu:
  main:
    parent: about
---
## ODBORNÉ FYZIOTERAPEUTICKÉ KURZY V RÁMCI POSTGRADUÁLNÍHO STUDIA
- **Spiraldynamik® Basic Med** (L. Kazmarová)
- **Dynamická neuromuskulární stabilizace podle Koláře®** (Šafářová, Ph.D, Čmolíková, Ujcová, Pintarová)
- **Proprioceptivní neuromuskulární facilitace®** (Bastlová, Ph.D)
- **Diagnostika a terapie funkčních poruch pohybového systému (Měkké a mobilizační techniky)** (MUDr. Poděbradský, doc. Jesenická, Ph.D)
- **Viscerální manipulace 1, 2, 4®** (Vey)
- **Klappovo lezení®** (Čápová)
- **Diagnostika a kinezioterapie u idiopatické skoliózy** (Pallová)
- **SM-systém skolióza** (Smíšková)
- **Fascia Distortion Model Modul 1** (Baumgartner, Breinessl)
- **Forma, funkce, facilitace** (Lewitová)
- **Pohyb a stabilita** (Vanclová, Vaĺa)
- **Mobilizace žeber metodou Ludmily Mojžíšové** (Hurtová)
- **Diagnostika a terapie ramenního kloubu** (Rutowicz, Ph.D)
- **Diagnostika a terapie kolenního kloubu** (Rutowicz, Ph.D)
- **Diagnostika a terapie závratí** (Čakrt, Ph.D)
- **Medical Taping Concept** (Krestová)
- **Stabilita v pohybovém systému a hluboký stabilizační systém** (Suchomel)
- **Propriofoot koncept** (I. Palaščáková Špringrová, Ph.D)
- **Vojtova reflexní lokomoce, část A®** (RL-Corpus, Olomouc) 
- **Redcord  - Základní kurz S-E-T** (Tomisová)

## Jiné (doplňkové) kurzy
- Respiratory Conference, Jeddah, Saudská Arábie 
- Global Osteopathic Approach, Hesham Khalil, Jeddah, Saudská Arábie 
- Basic Life Support, opakovaně každé 2 roky, American Heart Association, Jeddah, Saudská Arábie
- Stress Management, KFSHRC, Jeddah, Saudská Arábie 
- First Palliative Medicine Workshop, KFSHRC, Jeddah, Saudská Arábie Medical English, G5 Plus, Praha 
- Dornova metoda a Breussova masáž, Šupina, Brno
- Reflexologie včetně vertikální reflexní terapie, Šupina, Brno
- Pánevní dno pohledem Východu a Západu, Skálová, Praha
- Poruchy dechového stereotypu, Společnost pro myoskeletální medicínu, Praha
- Baňkování, Uríková, Brno 
- Využití jógy, tchai ji a čchi gung ve fyzioterapii, Švejcar, Praha
- Základy komplexního psychosomatického přístupu v rehabilitaci, Hnízdil, Strakonice
- Nordic walking, Šimková, Brno
